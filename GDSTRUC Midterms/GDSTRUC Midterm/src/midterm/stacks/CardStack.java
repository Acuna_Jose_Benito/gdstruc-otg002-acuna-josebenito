package midterm.stacks;

import java.util.EmptyStackException;

import java.util.Objects;

public class CardStack {
    private Card[] stack;
    private int top;
    private int totalCards;

    public CardStack(int capacity)
    {
        stack=new Card[capacity];
    }

    public void push(Card card)
    {
        if(top==stack.length)
        {
            Card[] newStack=new Card[2*stack.length];
            System.arraycopy(stack,0,newStack,0,stack.length);
            stack=newStack;
        }
        totalCards++;
        stack[top++]=card;
    }

    public Card pop()
    {
        if(isEmpty())
        {
            throw new EmptyStackException();
        }
        totalCards--;
        Card poppedCard=stack[--top];
        stack[top]=null;
        return poppedCard;
    }

    public Card peek()
    {
        if(top==stack.length)
        {
            Card[] newStack=new Card[2*stack.length];
            System.arraycopy(stack,0,newStack,0,stack.length);
            stack=newStack;
        }

        return stack[top-1];
    }

    public void printStack()
    {
        for (int i = top-1; i>=0; i--)
        {
            System.out.println(stack[i]);
        }
    }

    public boolean isEmpty()
    {
        return top==0;
    }

    public int getTotalCards() {
        return totalCards;
    }

    public void drawCards(int numberDraw, CardStack hand, CardStack deck)
    {
        for(int start=0;start<numberDraw;start++) //CONTINUE DRAWING 1-5 TIMES
        {
            if(deck.getTotalCards()<=0) //STOPS LOOP ONCE THERE ARE NO CARDS TO DRAW
            {
                System.out.println("Attempted to draw cards from the Deck but there are no more cards.");
                break;
            }

            hand.push(deck.peek()); //INSERTS TOP CARD FROM DECK TO HAND
            System.out.println("Player draws " + deck.peek() + " from the Draw Pile.");  //DISPLAYS CARD
            deck.pop(); //DELETES TOP CARD FROM DECK
        }
    }

    public void discardCards(int numberDraw, CardStack hand, CardStack discard)
    {
        for(int start=0;start<numberDraw;start++)
        {
            if(hand.getTotalCards()<=0)
            {
                System.out.println("Attempted to discard cards from the Hand but there are no more cards.");
                break;
            }

            discard.push(hand.peek());
            System.out.println("Player discards " + hand.peek() + " from their Hand.");
            hand.pop();
        }
    }

    public void drawDiscard(int numberDraw, CardStack hand, CardStack discard)
    {
        for(int start=0;start<numberDraw;start++)
        {
            if(discard.getTotalCards()<=0)
            {
                System.out.println("Attempted to draw from the Discard Pile but here are no more cards.");
                break;
            }

            hand.push(discard.peek());
            System.out.println("Player draws " + discard.peek() + " from the Discard Pile.");
            discard.pop();
        }
    }
}
