package midterm.stacks;

import java.util.Scanner;
import java.util.Random;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner scanner = new Scanner(System.in);
        Random rand=new Random();

        CardStack playerHand=new CardStack(30);
        CardStack discardPile=new CardStack(30);

        CardStack drawPile=new CardStack(30);
        drawPile.push(new Card(0,"The Fool"));
        drawPile.push(new Card(1,"The Magician"));
        drawPile.push(new Card(2,"The High Priestess"));
        drawPile.push(new Card(3,"The Empress"));
        drawPile.push(new Card(4,"The Emperor"));
        drawPile.push(new Card(5,"The Hierophant"));
        drawPile.push(new Card(6,"The Lovers"));
        drawPile.push(new Card(7,"Justice"));
        drawPile.push(new Card(8,"The Hermit"));
        drawPile.push(new Card(9,"Wheel of Fortune"));
        drawPile.push(new Card(10,"Strength"));
        drawPile.push(new Card(11,"The Hanged Man"));
        drawPile.push(new Card(12,"Death"));
        drawPile.push(new Card(13,"Temperance"));
        drawPile.push(new Card(14,"The Devil"));
        drawPile.push(new Card(15,"The Tower"));
        drawPile.push(new Card(16,"The Star"));
        drawPile.push(new Card(17,"The Moon"));
        drawPile.push(new Card(18,"The Sun"));
        drawPile.push(new Card(19,"Judgement"));
        drawPile.push(new Card(20,"The World"));
        drawPile.push(new Card(21,"Love-sadKid"));
        drawPile.push(new Card(22,"Hippo Campus"));
        drawPile.push(new Card(23,"Galdive"));
        drawPile.push(new Card(24,"Rare Americans"));
        drawPile.push(new Card(25,"Samm Henshaw"));
        drawPile.push(new Card(26,"Good Kid"));
        drawPile.push(new Card(27,"forrest."));
        drawPile.push(new Card(28,"Gen Hoshino"));
        drawPile.push(new Card(29,"nak"));

        while(drawPile.getTotalCards()>0)
        {
            int choice=rand.nextInt(3) + 1;
            int loopNum=rand.nextInt(5) + 1;

            //UNCOMMENT WHEN PLAER WANTS RIGHTS
            //System.out.println("1 = Draw from Deck    2 = Discard to Discard Pile    3 = Draw from Discard Pile");
            //System.out.println("What would you like to do? ");
            //int playChoice = scanner.nextInt();
            //choice=playChoice;

            if(choice==1)  //DRAWS 1-5 CARDS
            {
                playerHand.drawCards(loopNum,playerHand,drawPile);
            }

            else if(choice==2)  //DISCARD 1-5 CARDS
            {
                playerHand.discardCards(loopNum,playerHand,discardPile);
            }

            else if(choice==3)  //GET 1-5 CARDS FROM THE DISCARDS PILE
            {
                playerHand.drawDiscard(loopNum,playerHand,discardPile);
            }

            //DISPLAY FOLLOWING INFO
            System.out.println("Players' Hand (" + playerHand.getTotalCards() + " cards in total): ");
            playerHand.printStack();
            System.out.println("Number of cards in the Deck: " + drawPile.getTotalCards());
            System.out.println("Number of cards in the Discard Pile: " + discardPile.getTotalCards());
            scanner.nextLine();
            clearScreen();
            System.out.println("===================================================\n");
        }

        System.out.println("\nThere are no more cards in the Deck.");
    }

    public static void clearScreen() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }
}
