package gdstruc.module5;


public class Main {

    public static void main(String[] args) {
	// write your code here

        Player ploo=new Player(134,"Plooful",135);
        Player wardell=new Player(536,"TSM Wardell",635);
        Player deadlyJimmy=new Player(432,"DeadlyJimmy",4321);
        Player subroza=new Player(325,"Subroza",532);
        Player annieDro=new Player(235,"C9 Annie",155);

        SimpleHashtable hashtable=new SimpleHashtable();

        hashtable.put(ploo.getName(),ploo);
        hashtable.put(wardell.getName(), wardell);
        hashtable.put(deadlyJimmy.getName(), deadlyJimmy);
        hashtable.put(subroza.getName(), subroza);
        hashtable.put(annieDro.getName(), annieDro);

        hashtable.remove("Subroza");

        hashtable.printHashtable();
        //System.out.println(hashtable.get("Subroza"));
    }
}
