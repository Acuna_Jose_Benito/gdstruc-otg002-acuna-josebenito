package gdstruc.module4;

import java.util.Random;
import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        //QUEUE = FIRST IN FIRST OUT
        Scanner scanner = new Scanner(System.in);
        Random rand=new Random();

        int numPlayers=0;
        int gameNum=0;

        ArrayQueue queue=new ArrayQueue(10);

        //KEEP RUNNING UNTIL 10 GAMES HAVE BEEN STARTED
        while(gameNum<10)
        {
            scanner.nextLine();
            System.out.println("Total games found:" + gameNum + "      Total players currently in queue:" + queue.size());
            int playersJoin=rand.nextInt(7)+1;
            int counter=0;

            System.out.println(playersJoin + " has been added to the queue.");

            //ADDS NUMBER OF PLAYERS TO QUEUE
            while(counter<playersJoin)
            {
                int randPlayID=rand.nextInt(1000) + 1;
                queue.add(new Player(randPlayID));
                counter++;
                System.out.println(queue.cheat() + " joined the queue.");
            }

            //BEGINS ADDING PLAYERS TO GAME ONCE 5 PLAYERS ARE IN THE QUEUE (ADDS TO GAME COUNTER)
            while(queue.size()>=5)
            {
                int removeCounter=0;
                System.out.println("");
                while(removeCounter!=5)
                {
                    System.out.println(queue.peek() + " left to join the game.");
                    queue.remove();
                    removeCounter++;
                }
                gameNum++;
            }
            System.out.println("================================================");
        }

        System.out.println("\nA TOTAL OF 10 GAMES HAS BEEN CREATED.");

        System.out.println("Players left in queue:");
        queue.printQueue();
    }
}
