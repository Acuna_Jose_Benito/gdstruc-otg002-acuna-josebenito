package com.gdstruc.module1;

public class Main {

    public static void main(String[] args) {
	// write your code here
        int[] numbers=new int[10];

        numbers[0]=35;
        numbers[1]=-2;
        numbers[2]=103;
        numbers[3]=28;
        numbers[4]=55;
        numbers[5]=79;
        numbers[6]=10;
        numbers[7]=40;
        numbers[8]=0;
        numbers[9]=5;

        System.out.println("Before Sort: ");
        printArrayElements(numbers);

        System.out.println("\n\nAfter Selection Sort (Descending): ");
        selectionSort(numbers);
        printArrayElements(numbers);

        System.out.println("\n\nAfter Bubble Sort (Descending): ");
        bubbleSort(numbers);
        printArrayElements(numbers);
    }

    private static void bubbleSort(int[] arr)
    {
        //loop that slowly decreases as the elements are sorted
        for(int lastSortedIndex=arr.length-1;lastSortedIndex>0;lastSortedIndex--)
        {
            //loop that goes through each element and checks if the
            //current element is larger/smaller than the next element then swaps them
            for(int i=0;i<lastSortedIndex;i++)
            {
                if(arr[i]<arr[i+1])
                {
                    int temp=arr[i];
                    arr[i]=arr[i+1];
                    arr[i+1]=temp;
                }
            }
        }
    }

    private static void selectionSort(int[] arr)
    {
        for(int lastSortedIndex=arr.length-1;lastSortedIndex>0;lastSortedIndex--)
        {
            //titleholder for largest/smallest in the array
            int largestIndex=0;
            //loop that goes and compares through each element
            for(int i=1;i<=lastSortedIndex;i++)
            {
                //checks if current element is larger/smaller than the titleholder
                if(arr[i]<arr[largestIndex])
                {
                    //will not swap yet since it needs to check the entire array
                    largestIndex = i;
                }
            }
            //now it swaps with the last element in the array of the current loop
            int temp=arr[lastSortedIndex];
            arr[lastSortedIndex]=arr[largestIndex];
            arr[largestIndex]=temp;
        }
    }

    private static void printArrayElements(int[] arr)
    {
        //foreach loop
        for (int j : arr) {
            System.out.print(j+" ");
        }
    }
}
