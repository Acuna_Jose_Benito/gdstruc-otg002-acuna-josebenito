package linked.lists;

public class PlayerLinkedList {
    private PlayerNode head;
    private int playerCount=0;

     public void addToFront(Player player)
     {
         PlayerNode playerNode=new PlayerNode(player);
         playerNode.setNextPlayer(head);
         head=playerNode;
         playerCount++;
     }

     public void removeFirstElement()
     {
         //sets next node as head and disconnects previous head's reference to the list
         PlayerNode current=head;
         head=current.getNextPlayer();
         current.setNextPlayer(null);
         playerCount--;
     }

     public void printList()
     {
         PlayerNode current=head;
         System.out.print("HEAD -> ");

         while (current!=null)
         {
             //changed current to current.getPlayer(). Was displaying address instead of overriding.
             System.out.print(current.getPlayer());
             System.out.print(" -> ");
             current=current.getNextPlayer();
         }

         System.out.println("null");
     }

    public int getPlayerCount() {
        return playerCount;
    }

    public void containsFunc(Player player)
    {
        PlayerNode current=head;
        //loop that checks every node for input
        while (current!=null)
        {
            if(current.getPlayer().equals(player))
            {
                System.out.println("List contains specified node.\n");
                break;
            }
            current=current.getNextPlayer();
        }
    }

    public void indexOfFunc(Player player)
    {
        int element=0;
        PlayerNode current=head;
        //loop that check every node for input while keeping track of index
        while (current!=null)
        {
            if(current.getPlayer().equals(player))
            {
                System.out.println(player + " is at index " + element + ".\n");
                break;
            }
            element++;
            current=current.getNextPlayer();
        }
    }
}
