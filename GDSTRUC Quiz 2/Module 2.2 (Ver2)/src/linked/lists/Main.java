package linked.lists;

import java.util.ArrayList;
import java.util.List;
import java.util.LinkedList;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Player asuna=new Player(1,"Asuna",100);
        Player lethalBacon=new Player(2,"LethalBacon",205);
        Player hpDeskjet=new Player(3,"HPDeskjet",34);

        Player dumbs=new Player(4,"Dumbs",20);

        PlayerLinkedList playerLinkedList=new PlayerLinkedList();

        playerLinkedList.addToFront(asuna);
        playerLinkedList.addToFront(lethalBacon);
        playerLinkedList.addToFront(hpDeskjet);

        playerLinkedList.addToFront(dumbs);

        playerLinkedList.removeFirstElement();

        System.out.print("Player Count: ");
        System.out.println(playerLinkedList.getPlayerCount()+"\n");

        playerLinkedList.containsFunc(asuna);

        playerLinkedList.indexOfFunc(asuna);

        playerLinkedList.printList();
    }
}
